package org.automation.mobilepages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage{
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login(){
       keyword.waitUntilElementIsVisible(By.xpath("//*[contains(@resource-id,'button1')]")).click();
       keyword.waitUntilElementIsVisible(By.xpath("//*[contains(@resource-id,'btnSave')]")).click();
       sleepInSecond(2);
       keyword.findElement(By.xpath("//*[contains(@resource-id,'tvEmail')]")).click();
       sleepInSecond(2);
       keyword.findElement(By.xpath("//*[contains(@resource-id,'edtEmail')]")).click();
       keyword.findElement(By.xpath("//*[contains(@resource-id,'edtEmail')]")).sendKeys("abc");
       sleepInSecond(2);
//       keyword.hideKeyboard("android");
       keyword.hideKeyboard();
       sleepInSecond(2);
    }

    public void sleepInSecond(long numberInSecond) {
        try {
            Thread.sleep(numberInSecond * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
