package org.automation.mobilepages;

import org.automation.keywords.mobile.MobileKeywords;
import org.openqa.selenium.WebDriver;

public class BasePage {
    WebDriver driver;
    MobileKeywords keyword;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        keyword = new MobileKeywords(this.driver);

    }
}

