package org.automation.webpages;

import org.automation.config.TestData;
import org.automation.keywords.web.WebKeywords;
import org.automation.driver.DriverManager;

import java.util.HashMap;

public abstract class BasePage {

    protected WebKeywords keyword;

    protected BasePage() {
        keyword = new WebKeywords(DriverManager.getDriver());
    }

    protected HashMap<String, String> getPageLocator(String page){
        return (HashMap<String, String>) TestData.locators.get(page);
    }
}
