package org.automation.webpages;
import org.openqa.selenium.By;
import java.util.HashMap;

public final class OnboardingPage extends BasePage {
    private final HashMap<String, String> onboardingPageLocators;

    public OnboardingPage() {
        onboardingPageLocators = getPageLocator("OnboardingPage");
    }

    public OnboardingPage clickOnChosenCountriesDDL(){
        keyword.click(By.xpath(onboardingPageLocators.get("Choose_Country_DDL")));
        return this;
    }

    public OnboardingPage clickOnCountryDDL(){
        keyword.click(By.xpath(onboardingPageLocators.get("Estonia_DDL")));
        return this;
    }

    public OnboardingPage clickOnContinueBtn(){
        keyword.click(By.xpath(onboardingPageLocators.get("Continue_Btn")));
        return this;
    }

    public boolean isLoginLabelVisible(){
        try {
            keyword.waitUntilElementIsVisible(By.xpath(onboardingPageLocators.get("Login_label")));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
