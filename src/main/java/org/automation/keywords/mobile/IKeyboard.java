package org.automation.keywords.mobile;

public interface IKeyboard {

    void hideKeyboard(String browser);

    void hideKeyboard();
}
