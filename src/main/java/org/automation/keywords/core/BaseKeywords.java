package org.automation.keywords.core;

import org.automation.contants.FrameworkContants;
import org.openqa.selenium.*;
import org.openqa.selenium.internal.Require;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.logging.Logger;

/**
Author: dao chi quyen
Support:
 org.seleniumhq.selenium ver 4.18.1
 io.appium ver 9.2.2
*/
public abstract class BaseKeywords implements SearchContext, WaitContext, IAlert {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected WebElement element;
    protected JavascriptExecutor jsExecutor;
    protected Logger logger = Logger.getLogger(getClass().getName());

    protected BaseKeywords(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(FrameworkContants.getExplicitWaitTimeout()));
    }

    /**
     * Retrieves the WebDriver instance associated with this object.
     * @return the WebDriver instance
     */
    public WebDriver getDriver(){
        return this.driver;
    }

    /**
     * Finds the first web element using the given locator.
     * @param locator the locator strategy used to find the element
     * @return the first web element matching the given locator
     * @throws NoSuchElementException if no element matching the given locator is found
     */
    @Override
    public WebElement findElement(By locator) {
        Require.nonNull("Locator ", locator);
        try {
            return driver.findElement(locator);
        } catch (NoSuchElementException e) {
            logger.info("Element not found " + locator);
            throw e;
        }
    }

    /**
     * Finds all web elements using the given locator.
     * @param locator the locator strategy used to find the elements
     * @return a list of web elements matching the given locator
     * @throws NoSuchElementException if no elements matching the given locator are found
     */
    @Override
    public List<WebElement> findElements(By locator){
        Require.nonNull("Locator ", locator);
        try {
            return driver.findElements(locator);
        } catch (NoSuchElementException e) {
            logger.info("Elements not found " + locator);
            throw e;
        }
    }

    /**
     * Waits until the element located by the given locator is clickable.
     * @param locator the locator strategy used to find the element
     * @return the web element once it is clickable
     * @throws TimeoutException if the element is not clickable within the specified timeout period
     */
    @Override
    public WebElement waitUntilElementIsClickable(By locator) {
        Require.nonNull("Locator ", locator);
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(locator));
        } catch (TimeoutException e) {
            logger.info("Element not clickable " + locator);
            throw e;
        }
    }

    /**
     * Waits until the element located by the given locator is visible.
     * @param locator the locator strategy used to find the element
     * @return the web element once it is visible
     * @throws TimeoutException if the element is not visible within the specified timeout period
     */
    @Override
    public WebElement waitUntilElementIsVisible(By locator) {
        Require.nonNull("Locator ", locator);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            logger.info("Element not visible " + locator);
            throw e;
        }
    }

    /**
     * Waits until the element located by the given locator is present.
     * @param locator the locator strategy used to find the element
     * @return the web element once it is present
     * @throws TimeoutException if the element is not present within the specified timeout period
     */
    @Override
    public WebElement waitUntilElementIsPresent(By locator){
        Require.nonNull("Locator ", locator);
        try {
            return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (TimeoutException e) {
            logger.info("Element not present " + locator);
            throw e;
        }
    }

    @Override
    public String getAlertText() throws NoAlertPresentException {
        try {
            return driver.switchTo().alert().getText();
        } catch (NoAlertPresentException e) {
            logger.info("No alert present!");
            throw e;
        }
    }

    /**
     * Checks whether an alert is present.
     * @return True if an alert is present, false otherwise.
     * @throws NoAlertPresentException If no alert is present.
     */
    @Override
    public boolean isAlertPresent() throws NoAlertPresentException {
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            return true;
        } catch (NoAlertPresentException e) {
            logger.info("No alert present!");
            return false;
        }
    }

    /**
     * Waits until an alert is present.
     *
     * @throws NoAlertPresentException If no alert is present.
     */
    public void waitUntilAlertPresent() throws NoAlertPresentException {
        try {
            wait.until(ExpectedConditions.alertIsPresent());
        } catch (NoAlertPresentException e) {
            logger.info("No alert present!");
            throw e;
        }
    }

    /**
     * Accepts the alert present.
     * @throws NoAlertPresentException If no alert is present.
     */
    public void acceptAlert() throws NoAlertPresentException {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException e) {
            logger.info("No alert present!");
            throw e;
        }
    }

    /**
     * Accepts the alert present.
     * @throws NoAlertPresentException If no alert is present.
     */
    public void dismissAlert() throws NoAlertPresentException {
        try {
            driver.switchTo().alert().dismiss();
        } catch (NoAlertPresentException e) {
            logger.info("No alert present!");
            throw e;
        }
    }
}
