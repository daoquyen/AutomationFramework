package org.automation.contants;

public class FrameworkContants {

    private FrameworkContants(){

    }

    private static final String PATH = System.getProperty("user.dir");

    private static final String RESOURCE_PATH = PATH + "/src/test/resources";

    private static final String LOCATORS_PATH = RESOURCE_PATH + "/locators/web/ElementLocators.yml";

    private static final String EXISTING_USERS_PATH = RESOURCE_PATH + "/accounts/Test_Env_users.yml";

    private static final String CONFIG_PATH = RESOURCE_PATH + "/configs/config.properties";

    private static final int EXPLICIT_WAIT_TIMEOUT = 10;

    private static final int IMPLICIT_WAIT_TIMEOUT = 5;

    public static String getLocatorsPath(){
        return LOCATORS_PATH;
    }

    public static String getExistingUsersPath(){
        return EXISTING_USERS_PATH;
    }

    public static String getConfigPath(){
        return CONFIG_PATH;
    }

    public static String getResourcePath(){
        return RESOURCE_PATH;
    }

    public static int getExplicitWaitTimeout(){
        return EXPLICIT_WAIT_TIMEOUT;
    }

    public static int getImplicitWaitTimeout(){
        return IMPLICIT_WAIT_TIMEOUT;
    }

}
