package org.automation.helper.files;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class PropertiesHelper {

    private PropertiesHelper(){
    }

    public static Map<? , ?> getData(String filePath) {
        try (InputStream input = new FileInputStream(filePath)) {
            Properties prop = new Properties();
            prop.load(input);
            return new HashMap<>(prop);
        } catch (IOException e) {
            throw new RuntimeException("Error reading properties file: " + filePath + " " + e);
        }
    }

    @Deprecated
    public static Properties getProp(String filePath) {
        try (InputStream input = new FileInputStream(filePath)) {
            Properties prop = new Properties();
            prop.load(input);
            return prop;
        } catch (IOException e) {
            throw new RuntimeException("Error reading properties file: " + filePath + " " + e);
        }
    }

    @Deprecated
    public static String getValue(String key, String filePath) throws Exception {
        String value;
        try (InputStream input = new FileInputStream(filePath)) {
            Properties prop = new Properties();
            prop.load(input);
            value = prop.getProperty(key);
            if (Objects.isNull(value)) {
                throw new Exception("Property name " + key + " is not found!");
            }
            return value;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
