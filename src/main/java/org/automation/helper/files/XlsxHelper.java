package org.automation.helper.files;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class XlsxHelper {

    public static Object[] getDataForProvider(String sheetName, String filePath) throws IOException {
        Object[] data = null;

        try (InputStream input = new FileInputStream(filePath);
             Workbook workbook = new XSSFWorkbook(input)) {

            Sheet sheet = workbook.getSheet(sheetName);
            int rowNum = sheet.getPhysicalNumberOfRows() -1;
            int columnNum = sheet.getRow(0).getLastCellNum();

            data = new Object[rowNum];
            Map<String, String> map;

            for (int i = 1; i < columnNum; i++) {
                map = new HashMap<>();
                for (int j = 0; j < columnNum; j++) {
                    map.put(sheet.getRow(0).getCell(j).getStringCellValue()
                            , sheet.getRow(i).getCell(j).getStringCellValue());
                    data[i - 1] = map;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
