package org.automation.helper.files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class JsonHelper {

    public static Object[] getDataForProvider(String filepath) throws IOException {
        Map<String, Object> data = new ObjectMapper()
                .readValue(new File(filepath), new TypeReference<HashMap<String, Object>>() {});
        return new Object[] {data};
    }

    public static Object[] getArrayDataForProvider(String filepath) throws IOException {
        Object[] data = null;
        JSONParser jsonParser = new JSONParser();
        try {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(filepath));
            data = new Object[jsonArray.size()];
            for (int i=0; i<jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                Map<String, Object> map = jsonObjectToMap(jsonObject);
                data[i] = map;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static Map<String, Object> jsonObjectToMap(JSONObject jsonObject) {
        Map<String, Object> map = new HashMap<>();
        for (Object key : jsonObject.keySet()) {
            String keyStr = (String) key;
            Object keyVal = jsonObject.get(keyStr);
            map.put(keyStr, keyVal);
        }
        return map;
    }
}
