package org.automation.config;

import java.util.HashMap;
import org.automation.helper.files.YmlHelper;
import org.automation.contants.FrameworkContants;
import org.automation.helper.files.PropertiesHelper;

public class TestData {

    public static final HashMap<?, ?> locators;

    public static final HashMap<?, ?> existing_users;

    public static final HashMap<String, String> config;

    static {
        locators = YmlHelper.getData(FrameworkContants.getLocatorsPath());
        existing_users = YmlHelper.getData(FrameworkContants.getExistingUsersPath());
        config = (HashMap<String, String>) PropertiesHelper.getData(FrameworkContants.getConfigPath());
    }

    private TestData(){
    }
}
