package org.automation.listener;

import com.aventstack.extentreports.Status;
import org.automation.report.ReportTestManager;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

    @Override
    public void onStart(ITestContext context) {
        System.out.println("Test Suite " + context.getName() + " started");
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("Test Suite " + context.getName() + " finished");
        ReportTestManager.endTest();
    }

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Running test method " + result.getMethod().getMethodName() + " ...");
        ReportTestManager.startTest(result.getMethod().getMethodName(), result.getInstance().getClass().getName());
        ReportTestManager.getTest().info(result.getMethod().getMethodName() + " start successfully");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Executed " + result.getMethod().getMethodName() + " successfully");
        ReportTestManager.getTest().log(Status.PASS, "Test passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("Method " + result.getMethod().getMethodName() + " failed");
        ReportTestManager.getTest().log(Status.FAIL, "Test failed");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("Method " + result.getMethod().getMethodName() + " skipped");
        ReportTestManager.getTest().log(Status.SKIP, "Test skipped");
    }
}
