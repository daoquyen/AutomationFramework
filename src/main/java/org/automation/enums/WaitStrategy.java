package org.automation.enums;

public enum WaitStrategy {
    CLICKABLE,
    VISIBLE,
    PRESENCE,
    NONE
}
