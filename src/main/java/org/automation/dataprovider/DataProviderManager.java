package org.automation.dataprovider;

import org.automation.contants.FrameworkContants;
import org.automation.helper.files.XlsxHelper;
import org.automation.helper.files.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class DataProviderManager {

    @DataProvider
    public Object[] getDataForTestLoginFromXlsxFile() throws IOException {
        return XlsxHelper.getDataForProvider("testing", FrameworkContants.getResourcePath() + "/accounts/test_login_accounts.xlsx");
    }

    @DataProvider
    public Object[] getDataForTestLoginFromJsonFile() throws IOException {
        return JsonHelper.getDataForProvider(FrameworkContants.getResourcePath() + "/accounts/test_login_account.json");
    }

    @DataProvider
    public Object[] getArrayDataForTestLoginFromJsonFile() throws IOException {
        return JsonHelper.getArrayDataForProvider(FrameworkContants.getResourcePath() + "/accounts/test_login_accounts.json");
    }

    @DataProvider
    public Object[][] getDataForTestLoginWhileInvoiceModuleIsDisable(){
        return new Object[][] {
                {"qa_admin", "Testing1234!"},
                {"qa_admin2", "Testing123!"}
        };
    }
}
