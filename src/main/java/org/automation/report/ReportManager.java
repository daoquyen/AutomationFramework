package org.automation.report;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ReportManager {

    private static ExtentReports extent;

    private final static String reportName = "report";

    public static ExtentReports getInstance(){
        if (extent == null) {
            extent = new ExtentReports();
            ExtentSparkReporter spark = new ExtentSparkReporter("target/Report/"+ reportName +".html");
            spark.config().setTheme(Theme.STANDARD);
            spark.config().setEncoding("utf-8");
            extent.attachReporter(spark);
        }
        return extent;
    }
}
