package org.automation.report;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import java.util.HashMap;
import java.util.Map;

public class ReportTestManager {

    static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();

    static ExtentReports report = ReportManager.getInstance();

    public static synchronized ExtentTest getTest(){
        return extentTestMap.get((int)(Thread.currentThread().getId()));
    }

    public static synchronized void endTest(){
        report.flush();
    }

    public static synchronized ExtentTest startTest(String testName){
        ExtentTest test = report.createTest(testName);
        extentTestMap.put((int)(Thread.currentThread().getId()),test);
        return test;
    }

    public static synchronized void startTest(String testName, String className){
        ExtentTest test = report.createTest(testName, className);
        extentTestMap.put((int)(Thread.currentThread().getId()),test);
        test.assignCategory(className);
    }
}
