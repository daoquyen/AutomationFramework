package org.automation.webtests;

import org.automation.keywords.web.WebKeywords;
import org.automation.config.TestData;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.automation.driver.DriverManager;
import org.automation.driver.Driver;

public abstract class BaseTest {

    protected WebKeywords keyword;

    @BeforeMethod
    protected void setUp() throws Exception {

        Driver.initDriver("chrome");
        this.keyword = new WebKeywords(DriverManager.getDriver());
        keyword.openUrl(TestData.config.get("url"));
    }

    @AfterMethod
    protected void tearDown(){
        Driver.quitDriver();
    }
}
