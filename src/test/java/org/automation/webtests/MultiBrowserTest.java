//package org.automation.webtests;
//
//import org.automation.driver.BrowserFactory;
//import automationframework.support.keywords.web.WebKeywords;
//import org.testng.annotations.*;
//
//public class MultiBrowserTest extends BaseTest {
//
//    @Parameters ("browser")
//    @BeforeMethod (groups = {"Multi browser", "Demo"})
//    public void setup(String browser){
//        this.driver = BrowserFactory.setBrowser(browser);
//        keyword = new WebKeywords(driver);
//    }
//
//    @Test (groups = {"Multi browser", "Demo"})
//    public void testBrowser() throws Exception {
//        keyword.openUrl("https://test.fitek.in");
//    }
//
//    @AfterMethod (groups = {"Multi browser", "Demo"})
//    public void tearDown(){
//        driver.quit();
//    }
//}
