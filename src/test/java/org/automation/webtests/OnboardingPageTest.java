package org.automation.webtests;

import org.automation.webpages.OnboardingPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public final class OnboardingPageTest extends BaseTest {

    @Test
    public void isLoginTitleVisible(){
        OnboardingPage onboardingPage = new OnboardingPage();
        boolean isTitleVisible = onboardingPage
                                .clickOnChosenCountriesDDL()
                                .clickOnCountryDDL()
                                .clickOnContinueBtn()
                                .isLoginLabelVisible();
        Assert.assertTrue(isTitleVisible);
    }

    @Test
    public void isLoginTitleInvisible(){
        OnboardingPage onboardingPage = new OnboardingPage();
        boolean isTitleInvisible = onboardingPage
                                .clickOnChosenCountriesDDL()
                                .clickOnCountryDDL()
                                .clickOnContinueBtn()
                                .isLoginLabelVisible();
        Assert.assertFalse(isTitleInvisible);
    }

    @Test
    public void test(){

    }
}
