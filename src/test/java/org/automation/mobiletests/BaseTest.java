package org.automation.mobiletests;

import org.automation.driver.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.automation.keywords.mobile.MobileKeywords;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    WebDriver driver;
    MobileKeywords keyword;

    @BeforeMethod
    public void setUp() throws Exception {
        this.driver = BrowserFactory.setBrowser("android");
        this.keyword = new MobileKeywords(driver);
    }

    @AfterMethod
    public void tearDown(){
        this.driver.quit();
    }
}
