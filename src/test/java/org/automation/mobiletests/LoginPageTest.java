package org.automation.mobiletests;

import org.automation.mobilepages.LoginPage;
import org.testng.annotations.Test;

public class LoginPageTest extends BaseTest{
    @Test
    void isOpen(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login();
    }
}
