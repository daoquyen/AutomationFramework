package org.automation.frameworktest;

import org.automation.dataprovider.DataProviderManager;
import org.testng.annotations.Test;

import java.util.HashMap;

public class DataProviderTest {

    @Test(dataProvider = "getDataForTestLoginFromXlsxFile", dataProviderClass = DataProviderManager.class)
    public void dataProviderFromExcelFileTest(HashMap<String, String> map){
        System.out.println("Username " + map.get("username") + " Password " + map.get("password"));
    }

    @Test(dataProvider = "getDataForTestLoginFromJsonFile", dataProviderClass = DataProviderManager.class)
    public void dataProviderFromJsonFileTest(HashMap<String, Object> map){
        System.out.println(map);
    }

    @Test(dataProvider = "getArrayDataForTestLoginFromJsonFile", dataProviderClass = DataProviderManager.class)
    public void arrayDataProviderFromJsonFileTest(HashMap<String, Object> map){
        System.out.println(map);
    }

    @Test(dataProvider = "getDataForTestLoginWhileInvoiceModuleIsDisable", dataProviderClass = DataProviderManager.class)
    public void verifyInvoicePageIsNotAvailableWhenModuleDeactivedTest(String username, String password){
        System.out.println(username + " " + password);
    }
}
